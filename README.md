### Build & run project
# Build 
./gradlew build

# Run project
java -jar build/libs/anz-test.jar

# Test
./gradlew clean test

You can also grab the test reports in `build/reports/tests` folder