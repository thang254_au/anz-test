package com.thang.anztest;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class Application {
    public static void main( String[] args ) throws Exception {
        System.out.println( "Input RPN string:" );
        RPNCalculator calculator = new RPNCalculator();
        calculator.getInput();
    }
}
