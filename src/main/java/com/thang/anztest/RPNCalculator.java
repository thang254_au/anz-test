package com.thang.anztest;

import com.thang.anztest.operator.Operator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
import java.text.DecimalFormat;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class RPNCalculator {
    protected Stack<Double> numbers;
    protected Stack<Operator> operators;
    protected Stack<Stack<Double>> operationHistory;
    protected HashMap<String,String> operatorMap;
    protected String input;

    public RPNCalculator() {
        numbers = new Stack<Double>();
        operators = new Stack<Operator>();
        operatorMap = new HashMap();
        operationHistory = new Stack<Stack<Double>>();

        //create a map between operator and operator class name
        operatorMap.put("+","PlusOperator");
        operatorMap.put("-","MinusOperator");
        operatorMap.put("*","MultiplyOperator");
        operatorMap.put("/","DivideOperator");
        operatorMap.put("clear","ClearOperator");
        operatorMap.put("undo","UndoOperator");
        operatorMap.put("sqrt","SqrtOperator");
    }

    /**
     * Get Input from stdin
     * Loop until user terminate
     */
    public void getInput() throws Exception {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            this.input = scanner.nextLine();
            parseInput(this.input);

            //print the numbers stack
            System.out.println("Stack: " + this.stackToString(numbers));
        }
    }

    public boolean parseInput(String input) throws Exception {
        String data = "";
        for (int i=0;i<input.length();i++) {
            if (input.charAt(i) != ' ') {
                data += input.charAt(i);
            }

            if ((input.charAt(i) == ' ' || i == input.length() -1) && !data.equals("")) {
                //we got either a number or an operator
                if (NumberUtils.isNumber(data)) {
                    this.recordOperationHistory();
                    this.numbers.add(Double.parseDouble(data));
                } else {
                    //this is an operator
                    if (operatorMap.containsKey(data)) {
                        String className = operatorMap.get(data);

                        Operator operator = (Operator) Class.forName("com.thang.anztest.operator." + className).newInstance();
                        if (operator.isKeepingHistory()) {
                            this.recordOperationHistory();
                        }

                        operator.setNumbers(this.numbers);
                        operator.setOperationHistory(operationHistory);

                        if (!operator.operate()) {
                            System.out.println("operator "+operator.whoami()+" (position: "+i+"): insufficient parameters");
                            return false; //stop processing if there is an error
                        }
                    } else {
                        //Invalid operator
                        System.out.println("Invalid operator "+data+" (position: "+i+")");
                        return false; //stop processing if there is an error
                    }
                }
                data = "";
            }
        }

        return true;
    }

    public void recordOperationHistory() {
        Stack<Double> history = (Stack<Double>) numbers.clone();
        operationHistory.push(history);
    }

    public Stack<Stack<Double>> getOperationHistory() {
        return operationHistory;
    }

    public String stackToString(Stack<Double> numbers) {
        String result = "";
        for (int i=0;i<numbers.size();i++) {
            //+=String.format("%s",numbers.get(i));
            result+=new DecimalFormat("#.##########").format(numbers.get(i))+" ";
        }

        return result;
    }
}
