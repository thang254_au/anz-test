package com.thang.anztest.operator;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class ClearOperator extends Operator {
    public boolean operate() {
        this.numbers.removeAllElements();
        return true;
    }

    public Double calculate() {
        return null;
    }
    public String whoami() {
        return "Clear";
    }
    public int numberOfOperands() {
        return 0;
    }
}
