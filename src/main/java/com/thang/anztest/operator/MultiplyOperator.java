package com.thang.anztest.operator;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class MultiplyOperator extends Operator {
    public Double calculate() {
        return operands.pop() * operands.pop();
    }

    public String whoami() {
        return "*";
    }

    public int numberOfOperands() {
        return 2;
    }
}
