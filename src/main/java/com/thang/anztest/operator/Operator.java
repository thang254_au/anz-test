package com.thang.anztest.operator;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
public abstract class Operator {
    protected Stack<Double> numbers;
    protected Stack<Stack<Double>> operationHistory; //entire history of the numbers stack

    protected Stack<Double> operands = new Stack<Double>();

    /**
     * Pop 2 numbers from the stack, to operate on
     *
     * @return
     */
    public boolean popNumbers() {
        if (this.numbers.size() < numberOfOperands()){
            return false;
        }
        for (int i=0;i<this.numberOfOperands();i++) {
            this.operands.add(this.numbers.pop());
        }

        return true;
    }

    public void updateHistory() {

    }

    public boolean operate() {
        if (!popNumbers()) {
            return false;
        }
        updateHistory();
        Double result = calculate();

        //push the result back to the stack
        this.numbers.push(result);

        return true;
    }

    public void setNumbers(Stack<Double> numbers) {
        this.numbers = numbers;
    }

    public Stack<Double> getNumbers() {
        return numbers;
    }

    public Stack<Stack<Double>> getOperationHistory() {
        return operationHistory;
    }

    public void setOperationHistory(Stack<Stack<Double>> operationHistory) {
        this.operationHistory = operationHistory;
    }

    /**
     * Is this operator keep history
     * False for `undo`, true for all others.
     * @return
     */
    public boolean isKeepingHistory() {
        return true;
    }

    abstract public Double calculate();
    abstract public String whoami();
    abstract int numberOfOperands();
}
