package com.thang.anztest.operator;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class SqrtOperator extends Operator {
    public Double calculate() {
        return Math.sqrt(operands.pop());
    }

    public String whoami() {
        return "Sqrt";
    }

    public int numberOfOperands() {
        return 1;
    }
}
