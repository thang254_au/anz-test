package com.thang.anztest.operator;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class UndoOperator extends Operator {
    public boolean operate() {
        Stack<Double> previousState = this.operationHistory.pop();
        //copy all the elements from previous state of numbers stack to the current stact
        this.getNumbers().removeAllElements();
        while (!previousState.isEmpty()) {
            this.getNumbers().push(previousState.get(0));
            previousState.remove(0);
        }
        return true;
    }

    /**
     * Is this operator keep history
     * False for `undo`, true for all others.
     * @return
     */
    public boolean isKeepingHistory() {
        return false;
    }

    public Double calculate() {
        return null;
    }
    public String whoami() {
        return "Undo";
    }
    public int numberOfOperands() {
        return 0;
    }
}
