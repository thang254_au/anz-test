package com.thang.anztest;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by thangnguyen on 9/2/17.
 */
@RunWith(JUnitParamsRunner.class)
public class RPNCalculatorTest extends TestCase {
    @Test
    @Parameters({
            "1 2 3 4 5,true",
            "3 4 5 invalidOperator,false",
            "3 4 * / -,false"
    })
    public void testParseInput(String input, boolean expectedResult) throws Exception {
        RPNCalculator calculator = new RPNCalculator();
        this.assertEquals(expectedResult,calculator.parseInput(input));
    }
}