package com.thang.anztest.operator;

import junit.framework.TestCase;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
public class ClearOperatorTest extends TestCase {
    public void testOperate() throws Exception {
        Stack<Double> numbers = new Stack<Double>();
        numbers.push(new Double(5));
        numbers.push(new Double(3));

        ClearOperator operator = new ClearOperator();
        operator.setNumbers(numbers);
        operator.operate();
        this.assertEquals(0,operator.getNumbers().size());
    }
}