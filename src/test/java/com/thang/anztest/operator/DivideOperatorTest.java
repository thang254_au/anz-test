package com.thang.anztest.operator;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
@RunWith(JUnitParamsRunner.class)
public class DivideOperatorTest extends TestCase {
    @Test
    @Parameters({
        "6,3,2",
        "5,2,2.5"
    })
    public void testOperate(double operand1, double operand2, double testValue) throws Exception {
        Stack<Double> numbers = new Stack<Double>();
        numbers.push(new Double(operand1));
        numbers.push(new Double(operand2));

        DivideOperator operator = new DivideOperator();
        operator.setNumbers(numbers);
        operator.operate();

        Double result = operator.getNumbers().peek();
        this.assertEquals(0,result.compareTo(new Double(testValue)));
    }
}