package com.thang.anztest.operator;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Stack;

/**
 * Created by thangnguyen on 9/2/17.
 */
@RunWith(JUnitParamsRunner.class)
public class SqrtOperatorTest extends TestCase {

    @Test
    @Parameters({
        "9,3",
        "2.25,1.5"
    })
    public void testOperate(double operand1,double testValue) throws Exception {
        Stack<Double> numbers = new Stack<Double>();
        numbers.push(new Double(operand1));

        SqrtOperator operator = new SqrtOperator();
        operator.setNumbers(numbers);
        operator.operate();

        Double result = operator.getNumbers().peek();
        this.assertEquals(0,result.compareTo(new Double(testValue)));
    }
}