package com.thang.anztest.operator;
import com.thang.anztest.RPNCalculator;
import junit.framework.TestCase;
import java.util.Stack;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by thangnguyen on 9/3/17.
 */
public class UndoOperatorTest extends TestCase {
    public void testOperate() throws Exception {
        Stack<Stack<Double>> operationHistory = new Stack<Stack<Double>>();

        Stack<Double> numbers = new Stack<Double>();
        numbers.push(new Double(5));
        operationHistory.push((Stack<Double>) numbers.clone());
        numbers.push(new Double(3));
        operationHistory.push((Stack<Double>) numbers.clone());
        numbers.push(new Double(8));

        UndoOperator operator = new UndoOperator();
        operator.setNumbers(numbers);
        operator.setOperationHistory(operationHistory);
        this.assertEquals(3, operator.getNumbers().size());

        operator.operate();
        this.assertEquals(2,operator.getNumbers().size());
        operator.operate();
        this.assertEquals(1,operator.getNumbers().size());
    }
}